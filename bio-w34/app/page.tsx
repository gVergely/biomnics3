'use client';
import { useEffect, useState } from "react";


export default function Board() {

  const [inpValue, setInpValue] = useState("");
  const [result, setResult] = useState<any>();

  function handleButtonClick() {
    fetch('/api/search/' + inpValue)
    .then(response => {
      // Verificar si la respuesta es exitosa (código de estado 200)
      if (response.ok) {
        // Parsear la respuesta JSON
        return response.json();
      }
      // Si la respuesta no es exitosa, lanzar un error
      throw new Error('Network response was not ok.');
    })
    .then(data => {
      // Manejar los datos recibidos
      setResult(data.hits);

      // Aquí puedes hacer lo que necesites con los datos
    })
    .catch(error => {
      // Manejar errores en la solicitud fetch
      console.error('There was a problem with the fetch operation:', error);
    });
  }
    return (
      <main className="p-6">
        <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
          <input
            type="text"
            value={inpValue}
            onChange={(e) => setInpValue(e.target.value)}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            placeholder="Enter accession ID"
          />
          <button
            type="button"
            onClick={handleButtonClick}
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mt-4"
          >
            Search
          </button>
        </div> 
        <div className="border">
          <div className="flex justify-center">
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-gray-50">
                <tr>
                  <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Title</th>
                  <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Score</th>
                  <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">E-value</th>
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                {result && result.map((item: { title: string; score: number, evalue: number }, index: number) => (
                  <tr key={index}>
                    <td className="px-6 py-4 whitespace-nowrap">{item.title}</td>
                    <td className="px-6 py-4 whitespace-nowrap">{item.score}</td>
                    <td className="px-6 py-4 whitespace-nowrap">{item.evalue}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </main>
    );

}