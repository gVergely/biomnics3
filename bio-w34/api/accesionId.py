from Bio import Entrez
import os

CWD = os.getcwd()
def fetch_protein_sequence(accession_number):
    Entrez.email = "gabriel.vergely@gmail.com"

    handle = Entrez.efetch(db="protein", id=accession_number, rettype="fasta", retmode="text")

    fasta = f'{CWD}/blast/queries/{accession_number}.fasta'
    with open(fasta, "w") as f:
        f.write(handle.read())

    handle.close()
