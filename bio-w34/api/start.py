import os

def createDirs():
    main_directory = "blast"

    # Lista de subdirectorios a crear
    subdirectories = ["blastdb", "blastdb_custom", "fasta", "queries", "results"]

    # Crea los subdirectorios
    for subdir in subdirectories:
        # Ruta completa del subdirectorio
        subdirectory_path = os.path.join(main_directory, subdir)
        
        # Verifica si el subdirectorio ya existe
        if not os.path.exists(subdirectory_path):
            # Crea el subdirectorio
            os.makedirs(subdirectory_path)

    print("Directorios creados con éxito.")