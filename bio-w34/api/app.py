from flask import Flask
from flask_cors import CORS
from accesionId import fetch_protein_sequence
from compareSeq import *


app = Flask(__name__, static_folder="out", static_url_path="/")
CORS(app)


@app.route("/")
def index():
    createDirs()
    return app.send_static_file("index.html")


@app.route("/api/search/<accession_id>")
def search(accession_id): 
    result = fetch_protein_sequence(accession_id)
    if (result):
        blast(accession_id)
        data = obtainData(accession_id)

    return {"hits": data}

if __name__ == "__main__":
    app.run()

