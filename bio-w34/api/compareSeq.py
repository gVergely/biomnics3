import json
import os
from python_on_whales import docker
import re
import os.path as path



CWD = os.getcwd()

#Creates the necessary directories.
def createDirs():
    main_directory = "blast"

    subdirectories = ["blastdb", "blastdb_custom", "fasta", "queries", "results"]

    for subdir in subdirectories:
        subdirectory_path = os.path.join(main_directory, subdir)
        
        if not os.path.exists(subdirectory_path):
            os.makedirs(subdirectory_path)

#Obtains especific data(title, score, evalue) from the blast() output 
def obtainData(accession_id):
    with open(f'{CWD}/blast/results/{accession_id}.json', 'r') as file:
        data = json.load(file)

    hits = data['BlastOutput2'][0]['report']['results']['search']['hits']
    result = []
    for hit in hits:
        obj = {
            'title': hit['description'][0]['title'],
            'score': hit['hsps'][0]['score'],
            'evalue': hit['hsps'][0]['evalue']
        }
        result.append(obj)

    return result

#Returns a document with all the data returned after compare seuences.
def blast(accession_id):
    existsJson = path.exists(f'{CWD}/blast/results/{accession_id}.json')
    if (existsJson != True):
        output = docker.run(
            image="ncbi/blast",
            remove=True,
            volumes=[
                (f"{CWD}/blast/queries", "/queries"),
                (f"{CWD}/blast/blastdb", "/blast/blastdb"),
                (f"{CWD}/blast/results", "/blast/results")  # Agregamos el volumen para los resultados
            ],
            command=["blastp", "-query", f"/queries/{accession_id}.fasta", "-db", "pdbaa", "-out", f"/blast/results/{accession_id}.json", "-outfmt", "15"]  # Añadimos el formato de salida para el archivo JSON
        )
        return True