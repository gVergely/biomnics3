# Bio Sequence

```sh
docker run --rm -d --name sequence -p 80:80 registry.gitlab.com/xtec/bio-sequence
```

## Develop

Install dependencies:

```sh
./init.sh
source ~/.bashrc
```


Run:

```sh
npm run dev
```

or:

```sh
npm run next-dev
npm run flask-dev
```

## No he podido descargar el pdbaa con un script lo he hecho manualmente:

Entrar en el docker interactivo:

```
docker run --rm -it -v $PWD/blast/blastdb:/blast/blastdb -v $PWD/blast/blastdb_custom:/blast/blastdb_custom -v $PWD/blast/fasta:/blast/fasta -v $PWD/blast/queries:/blast/queries -v $PWD/blast/results:/blast/results ncbi/blast /bin/bash
```

moverse a blastdb:

```
cd blastdb
```

descargar:

```
update_blastdb.pl -source gcp pdbaa
```